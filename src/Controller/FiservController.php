<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Writer\PngWriter;


/**
 * @Route("/fiserv", name="fiserv_")
 */
class FiservController extends AbstractController
{
    /**
     * @Route("/qr", name="qr")
     */
    public function index(): Response
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $date = date("ymdHis", time() + 63);

        return new Response('00020101021243140010com.fiserv50150011305067300385126002200000284000000783297135204541153030325406204.005802AR5910La Anonima6005JUNIN6108B6000GAA624801010052160202161165402087384707086020216108020080450010com.fiserv010502.010202010312'.$date.'82930005ecc010180MEQCIEpaDZRxA0EnWsnPdRCxXDzdInTyNEvg0xV3lJkWKymDAiAip4ApnfnbwB+OurPodSEdYDlwCrXS83290005ecc020116BUp4bZX/pMKF6w==63042fa5');
    }

    /**
     * @Route("/qr.png", name="qr_jpg")
     */
    public function png(): Response
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $date = date("ymdHis", time() + 60);
        $qr = '00020101021243140010com.fiserv50150011305067300385126002200000284000000783297135204541153030325406204.005802AR5910La Anonima6005JUNIN6108B6000GAA624801010052160202161165402087384707086020216108020080450010com.fiserv010502.010202010312'.$date.'82930005ecc010180MEQCIEpaDZRxA0EnWsnPdRCxXDzdInTyNEvg0xV3lJkWKymDAiAip4ApnfnbwB+OurPodSEdYDlwCrXS83290005ecc020116BUp4bZX/pMKF6w==63042fa5';

        $result = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data($qr)
            ->build();

        $response = new Response();
        $response->setContent($result->getString());
        $response->headers->set('Content-Type', 'image/png');

        return $response;
    }

}
